# payment.transfer

Add following PSR-4 to laravel composer.json<br />
`"Dreamsoft\\Payment\\Transfer\\": "packages/payments/payment.transfer/src"`

Like this:<br />
`"autoload": {
        "classmap": [
            "database"
        ],
        "psr-4": {
            "App\\": "app/",
            "Dreamsoft\\Template\\Base\\": "packages/templates/template.base/src",
            "Dreamsoft\\Order\\vServerBasic\\": "packages/order/vserver.basic/src",
            "Dreamsoft\\Payment\\Transfer\\": "packages/payments/payment.transfer/src"
        }
    },`
   
  