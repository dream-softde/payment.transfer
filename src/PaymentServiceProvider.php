<?php

namespace Dreamsoft\Payment\Transfer;

use Illuminate\Support\ServiceProvider;

class PaymentServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/views', 'payment_transfer');

        //INSERT SETTINGS
        \App\Helpers\SettingHelper::addSettingEntry('payment.transfer', 'Einstellungen für die Überweisungen', [
            'account_holder' => 'Demo Firma GmbH',
            'account_iban' => 'DEXX XXXXXX XXXX XXXX',
            'account_bic' => 'XJSDDSF',
            'account_subject' => '%CUSTOMER_ID%'
        ], 'payment.ds.transfer', 'json');

        \App\Helpers\PackageHelper::registerPayment('payment_transfer');
    }

    public function register()
    {
    }
}
