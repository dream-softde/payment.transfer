<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Überweisung</h3>
    </div>
    <div class="panel-body">
        Kontoinhaber: {{ \App\Helpers\SettingHelper::getSetting('payment.transfer')->account_holder }}<br />
        IBAN: {{ \App\Helpers\SettingHelper::getSetting('payment.transfer')->account_iban }}<br />
        BIC: {{ \App\Helpers\SettingHelper::getSetting('payment.transfer')->account_bic }}<br />
        Verwendungszweck: {{ str_replace('%CUSTOMER_ID%', \App\Helpers\PackageHelper::getCustomerId(), \App\Helpers\SettingHelper::getSetting('payment.transfer')->account_subject) }}<br />
    </div>
</div>

